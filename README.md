# Setting up the CivalCraft++ Modpack

This guide will walk you through the process of importing a zip file as an instance in Poly MC and CurseForge. It will also provide a download for the world as of (Sept 7th. 2023)

## Importing a Zip File as an Instance
1. **Download and Install Poly MC / Curseforge**:
   - If you haven't already, download and install a launcher from the official website: 
   - [Poly MC](https://github.com/PolyMC/PolyLauncher)
   - [CurseForge](https://www.curseforge.com/download/app)

2. **Download the modpack**:
    - [modpack](modpack.zip)

3. **Create a New Instance**:
   - Click on the "Instances" tab.

4. **Import Modpack**:
   - Click the "Import Modpack" button. (It might be on the bottom of the screen for curseforge)

5. **Choose Your Zip File**:
   - Navigate to the location where you have downloaded the zip file of the modpack you want to import.
   - Select the zip file and click "Open."

6. **Configure Instance Options**:
   - Configure any instance options such as Minecraft version, RAM allocation, and Java version.
   - Set the name to "CivalCraft++" (You shouldnt need to if you're on curseforge)

7. **Create Instance**:
   - Click the "Create Instance" button.

8. **Play the Modpack**:
   - Your new modpack instance will be listed on the modpacks screen.
   - Click "Play" to start the modpack.

## Setting up the world

The world size is 6.6gb, so be aware of that.

1. **Download the world**:
   - [world](world.zip)

2. **Extract the world**:
    - Extract the world to your desktop or somewhere you can easily find it.
    - The folder should be called "world" but can be renamed to CivalCraft if you want.

3. **Open the instance folder**:
    - Click on the "Instances" tab.
    - Open the isntance folder for the modpack.

4. **Open the saves folder**:
    - Move the minecraft world to the saves folder.

5. **Congrats!**:
    - You should now be able to play the world in the modpack.